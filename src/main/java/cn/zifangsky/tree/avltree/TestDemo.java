package cn.zifangsky.tree.avltree;

import java.util.Arrays;
import java.util.List;

/**
 * AVL树基本用法的测试用例
 *
 * @author zifangsky
 * @date 2018/11/29
 * @since 1.0.0
 */
public class TestDemo {

    public static void main(String[] args) {
        //定义一个AVL树的实例
        AvlTree<Integer> avlTree = new AvlTree<>();
        //定义一些节点示例
        List<Integer> numbers = Arrays.asList(5,4,3,2,1,6,7,8,9,10);
        //将这些树插入到AVL树
        avlTree.insert(numbers);

        //遍历
        avlTree.forEach((element,height) -> {
            System.out.println("Node:" + element + "<---------->" + "Height:" + height);
        });

        //删除节点10
        avlTree.remove(10);
        //删除此时的最大节点（9）和最小节点（1）
        avlTree.removeMax();
        avlTree.removeMin();

        System.out.println("此时最小元素：" + avlTree.findMin());
        System.out.println("此时最大元素：" + avlTree.findMax());
        System.out.println("此时元素 1 是否存在：" + avlTree.contains(1));
        System.out.println("此时元素 2 是否存在：" + avlTree.contains(2));
        System.out.println("此时的AVL树是否为空：" + avlTree.isEmpty());
        //清空AVL树
        avlTree.clear();

    }
}
