package cn.zifangsky.tree.binarysearchtree;

/**
 * 二叉搜索树的单个节点定义
 * @author zifangsky
 *
 * @param <K>
 */
public class BinarySearchTreeNode<K extends Object> {
	private K data; //数据
	private BinarySearchTreeNode<K> left; //左孩子节点
	private BinarySearchTreeNode<K> right; //右孩子节点

	public BinarySearchTreeNode(K data) {
		this.data = data;
	}

	public BinarySearchTreeNode(K data, BinarySearchTreeNode<K> left, BinarySearchTreeNode<K> right) {
		this.data = data;
		this.left = left;
		this.right = right;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	public BinarySearchTreeNode<K> getLeft() {
		return left;
	}

	public void setLeft(BinarySearchTreeNode<K> left) {
		this.left = left;
	}

	public BinarySearchTreeNode<K> getRight() {
		return right;
	}

	public void setRight(BinarySearchTreeNode<K> right) {
		this.right = right;
	}

}
