package cn.zifangsky.tree.generictree;

/**
 * 通用树的单个节点定义
 * @author zifangsky
 *
 * @param <K>
 */
public class TreeNode<K extends Object> {

	private K data; // 数据
	private TreeNode<K> firstChild; // 第一个孩子节点
	private TreeNode<K> nextSibling; // 下一个兄弟节点

	public TreeNode(K data) {
		this.data = data;
	}

	public TreeNode(K data, TreeNode<K> firstChild, TreeNode<K> nextSibling) {
		this.data = data;
		this.firstChild = firstChild;
		this.nextSibling = nextSibling;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	public TreeNode<K> getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(TreeNode<K> firstChild) {
		this.firstChild = firstChild;
	}

	public TreeNode<K> getNextSibling() {
		return nextSibling;
	}

	public void setNextSibling(TreeNode<K> nextSibling) {
		this.nextSibling = nextSibling;
	}

}
