package cn.zifangsky.tree.binarytree;

/**
 * 二叉树的单个节点定义
 * @author zifangsky
 *
 * @param <K>
 */
public class BinaryTreeNode<K extends Object> {
	private K data; // 数据
	private BinaryTreeNode<K> left; //左孩子节点
	private BinaryTreeNode<K> right; //右孩子节点
	
	public BinaryTreeNode(K data) {
		this.data = data;
	}

	public BinaryTreeNode(K data, BinaryTreeNode<K> left, BinaryTreeNode<K> right) {
		this.data = data;
		this.left = left;
		this.right = right;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	public BinaryTreeNode<K> getLeft() {
		return left;
	}

	public void setLeft(BinaryTreeNode<K> left) {
		this.left = left;
	}

	public BinaryTreeNode<K> getRight() {
		return right;
	}

	public void setRight(BinaryTreeNode<K> right) {
		this.right = right;
	}

}
