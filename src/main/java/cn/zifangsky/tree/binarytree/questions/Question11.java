package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 求二叉树直径的算法
 * 树的直径：树中两个叶子节点之间的最长路径中的节点个数
 * @author zifangsky
 *
 */
public class Question11 {
	
	/**
	 * 求二叉树的直径
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public int getDiameter(BinaryTreeNode<Integer> root){
		if(root == null){
			return 0;
		}
		
		int leftHeight = getHeight(root.getLeft()); //左子树的高度
		int rightHeight = getHeight(root.getRight()); //右子树的高度
		
		return leftHeight + 1 + rightHeight;

	}
	
	/**
	 * 递归求二叉树的高度
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public int getHeight(BinaryTreeNode<Integer> root){
		if(root == null){
			return 0;
		}
		
		int leftHeight = getHeight(root.getLeft()); //左子树的高度
		int rightHeight = getHeight(root.getRight()); //右子树的高度
		
		if(leftHeight > rightHeight){
			return leftHeight + 1;
		}else{
			return rightHeight + 1;
		}

	}

	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("二叉树的直径是：" + getDiameter(root));
	}

}
