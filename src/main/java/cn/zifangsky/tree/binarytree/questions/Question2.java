package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 求二叉树中叶子节点的个数
 * @author zifangsky
 *
 */
public class Question2 {

	/**
	 * 通过递归遍历获取叶子节点个数
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public int getNumberOfLeavesByPreOrder(BinaryTreeNode<Integer> root){
		if(root == null){
			return 0;
		}else{
			if(root.getLeft() == null && root.getRight() == null){ //叶子节点
				return 1;
			}else{ //左子树叶子节点总数 + 右子树叶子节点总数
				return getNumberOfLeavesByPreOrder(root.getLeft()) + getNumberOfLeavesByPreOrder(root.getRight());
			}
		}

	}
	
	
	/**
	 * 使用层次遍历获取二叉树叶子节点个数
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public int getNumberOfLeavesByQueue(BinaryTreeNode<Integer> root){
		int count = 0; //叶子节点总数
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		if(root != null){
			queue.push(root);
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			//叶子节点：左孩子节点和右孩子节点都为NULL的节点
			if(temp.getLeft() == null && temp.getRight() == null){
				count++;
			}else{
				if(temp.getLeft() != null){
					queue.push(temp.getLeft());
				}
				if(temp.getRight() != null){
					queue.push(temp.getRight());
				}
			}
		}
		return count;
	}
	
	
	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("叶子节点个数是：" + getNumberOfLeavesByPreOrder(root));
		System.out.println("叶子节点个数是：" + getNumberOfLeavesByQueue(root));
		
	}

}
