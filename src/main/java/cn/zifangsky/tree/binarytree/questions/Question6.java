package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.queue.Queue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 求树的高度（深度），设：叶子节点的高度为1
 * @author zifangsky
 *
 */
public class Question6 {

	/**
	 * 递归求树的高度
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public int getHeight(BinaryTreeNode<Integer> root){
		if(root == null){
			return 0;
		}
		
		int leftHeight = getHeight(root.getLeft()); //左子树的高度
		int rightHeight = getHeight(root.getRight()); //右子树的高度
		
		if(leftHeight > rightHeight){
			return leftHeight + 1;
		}else{
			return rightHeight + 1;
		}

	}
	
	/**
	 * 使用层次遍历求树的高度
	 * 
	 * @时间复杂度 O(n)
	 * @return
	 */
	public int getHeightByQueue(BinaryTreeNode<Integer> root){
		int level = 0;
		
		Queue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		if(root != null){
			queue.push(root);
			queue.push(null); //每层结束时为下一层添加一个结束标识
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			
			if(temp != null){ //正常二叉树的某个节点
				if(temp.getLeft() != null){
					queue.push(temp.getLeft());
				}
				if(temp.getRight() != null){
					queue.push(temp.getRight());
				}
			}else{ //遍历到每层结束的结束标识时：level加一；如果不是最后一层则为下一层添加一个结束标识
				level++;
				if(!queue.isEmpty()){
					queue.push(null); //为下一层插入一个数据域为NULL的节点
				}
			}

		}

		return level;
	}
	
	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}
		
		System.out.println("树的高度是：" + getHeight(root));
		System.out.println("树的高度是：" + getHeightByQueue(root));
	}

}
