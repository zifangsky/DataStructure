package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 在二叉树中查找某个元素
 * @author zifangsky
 *
 */
public class Question4 {
	
	/**
	 * 通过递归前序遍历在二叉树中查找某个元素
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public boolean findElementByPreOrder(BinaryTreeNode<Integer> root,int data){
		
		if(root == null){
			return false;
		}else{
			if(root.getData().intValue() == data){
				return true;
			}else{
				return findElementByPreOrder(root.getLeft(), data) || findElementByPreOrder(root.getRight(), data);
			}	
		}
	}
	
	
	/**
	 * 通过层次遍历在二叉树中查找某个元素
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public boolean findElementByQueue(BinaryTreeNode<Integer> root,int data){
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		if(root != null){
			queue.push(root);
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			
			if(temp.getData().intValue() == data){
				return true;
			}else{
				if(temp.getLeft() != null){
					queue.push(temp.getLeft());
				}
				if(temp.getRight() != null){
					queue.push(temp.getRight());
				}
			}
		}
		return false;
	}
	
	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("-1是否在二叉树中存在： " + findElementByPreOrder(root,-1));
		System.out.println("5是否在二叉树中存在： " + findElementByPreOrder(root,5));
		
		System.out.println("8是否在二叉树中存在： " + findElementByQueue(root,8));

	}

}
