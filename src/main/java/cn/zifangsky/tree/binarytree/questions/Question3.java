package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 查找二叉树中最大元素
 * @author zifangsky
 *
 */
public class Question3 {

	/**
	 * 通过递归前序遍历查找二叉树中最大元素
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public Integer findMaxByPreOrder(BinaryTreeNode<Integer> root){
		Integer max = null; //最大值
		
		if(root != null){
			Integer rootMax = root.getData();
			Integer leftMax = findMaxByPreOrder(root.getLeft()); //左子树最大元素
			Integer rightMax = findMaxByPreOrder(root.getRight()); //右子树最大元素

			if(leftMax == null){
				max = rightMax;
			}else if(rightMax == null){
				max = leftMax;
			}else{
				if(leftMax.intValue() > rightMax.intValue()){
					max = leftMax;
				}else{
					max = rightMax;
				}
			}
			
			if((max == null) || (rootMax.intValue() > max.intValue())){
				max = rootMax;
			}
		}
		
		return max;

	}
	
	
	/**
	 * 通过层次遍历查找二叉树中最大元素
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public Integer findMaxByQueue(BinaryTreeNode<Integer> root){
		Integer max = null; //最大值
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		if(root != null){
			max = root.getData();
			queue.push(root);
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			
			if(temp.getData().intValue() > max.intValue()){
				max = temp.getData();
			}
			
			if(temp.getLeft() != null){
				queue.push(temp.getLeft());
			}
			if(temp.getRight() != null){
				queue.push(temp.getRight());
			}
			
		}
		return max;
	}
	
	
	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("二叉树中最大值是：" + findMaxByPreOrder(root));
		System.out.println("二叉树中最大值是：" + findMaxByQueue(root));
		
	}

}
