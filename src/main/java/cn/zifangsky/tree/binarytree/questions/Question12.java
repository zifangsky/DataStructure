package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.queue.Queue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 求二叉树宽度的算法
 * 树的宽度：二叉树各层节点个数的最大值
 * @author zifangsky
 *
 */
public class Question12 {
	
	/**
	 * 使用层次遍历获取二叉树的宽度
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public int getWidthByQueue(BinaryTreeNode<Integer> root){
		int maxWidth = 0; //各层节点个数的最大值
		Queue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		
		if(root != null){
			queue.push(root);
			maxWidth = 1;
			queue.push(null); //每层结束时为下一层添加一个结束标识
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			
			if(temp != null){ //正常二叉树的某个节点
				if(temp.getLeft() != null){
					queue.push(temp.getLeft());
				}
				if(temp.getRight() != null){
					queue.push(temp.getRight());
				}
			}else{ //遍历到每层结束的结束标识时：如果不是最后一层则为下一层添加一个结束标识
				int nextLevelWidth = queue.size(); //下一层的节点个数
				if(nextLevelWidth > maxWidth){
					maxWidth = nextLevelWidth;
				}
				
				if(!queue.isEmpty()){
					queue.push(null); //为下一层插入一个数据域为NULL的节点
				}
			}

		}

		return maxWidth;
	}

	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("二叉树的宽度是是：" + getWidthByQueue(root));
	}

}
