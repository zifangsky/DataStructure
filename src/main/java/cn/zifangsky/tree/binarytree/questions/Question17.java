package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 设计算法：打印二叉树中某个节点的所有祖先节点
 * @author zifangsky
 *
 */
public class Question17 {

	/**
	 * 打印某节点的所有祖先节点
	 * 
	 * @时间复杂度 O(n)
	 * @param root 根节点
	 * @param targetNode 目标节点
	 * @return
	 */
	public boolean printAllAncestors(BinaryTreeNode<Integer> root,BinaryTreeNode<Integer> targetNode){
		if(root == null){
			return false;
		}

		if(root.getLeft() == targetNode || root.getRight() == targetNode
				|| printAllAncestors(root.getLeft(),targetNode)
				|| printAllAncestors(root.getRight(),targetNode)){
			System.out.print(root.getData() + " ");
			return true;
		}
			
		return	false;
	}

	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		BinaryTreeNode<Integer> targetNode = null; //目标节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i == 4){
				targetNode = tmpNode1;
			}else{
				queue.push(tmpNode1);
			}
			queue.push(tmpNode2);
		}

		//打印数字4所在节点的所有祖先节点
		printAllAncestors(root,targetNode);
	
	}

}
