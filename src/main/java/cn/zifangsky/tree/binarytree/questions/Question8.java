package cn.zifangsky.tree.binarytree.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.queue.Queue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;

/**
 * 查找二叉树中最深节点
 * @author zifangsky
 *
 */
public class Question8 {
	
	/**
	 * 查找二叉树中最深节点
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public BinaryTreeNode<Integer> levelOrderInReverse(BinaryTreeNode<Integer> root){
		Queue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();

		if(root == null){
			return null;
		}
		queue.push(root);
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop();

			if(temp != null){ //正常二叉树的某个节点
				if(temp.getLeft() != null){
					queue.push(temp.getLeft());
				}
				if(temp.getRight() != null){
					queue.push(temp.getRight());
				}
				
				if(queue.isEmpty()){
					return temp;
				}
			}
			
		}
		
		return null;
	}

	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		BinaryTreeNode<Integer> result = levelOrderInReverse(root);
		System.out.println(result.getData());
	}

}
