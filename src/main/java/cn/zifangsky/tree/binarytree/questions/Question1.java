package cn.zifangsky.tree.binarytree.questions;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.tree.binarytree.BinaryTreeNode;
import org.junit.Test;

/**
 * 求二叉树中所有节点的个数
 * @author zifangsky
 *
 */
public class Question1 {

	/**
	 * 通过递归前序遍历获取所有节点个数
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 * @return
	 */
	public int getNumbersByPreOrder(BinaryTreeNode<Integer> root){
		if(root == null){
			return 0;
		}else{ //1 + 左子树节点总数 + 右子树节点总数
			return 1 + getNumbersByPreOrder(root.getLeft()) + getNumbersByPreOrder(root.getRight());
		}

	}
	
	
	/**
	 * 使用层次遍历获取所有节点个数
	 * 
	 * @时间复杂度 O(n)
	 * @param root
	 */
	public int getNumbersByQueue(BinaryTreeNode<Integer> root){
		int count = 0; //节点总数
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<>();
		if(root != null){
			queue.push(root);
		}
		
		while (!queue.isEmpty()) {
			BinaryTreeNode<Integer> temp = queue.pop(); //出队
			
			count++; //总数加1
			
			if(temp.getLeft() != null){
				queue.push(temp.getLeft());
			}
			if(temp.getRight() != null){
				queue.push(temp.getRight());
			}
			
		}
		return count;
	}
	
	
	/**
	 * 测试用例
	 */
	@Test
	public void testMethods(){
		/**
		 * 使用队列构造一个供测试使用的二叉树
		 *     1
		 *   2    3
		 * 4  5  6  7
		 *   8 9  
		 */
		LinkQueue<BinaryTreeNode<Integer>> queue = new LinkQueue<BinaryTreeNode<Integer>>();
		BinaryTreeNode<Integer> root = new BinaryTreeNode<>(1); //根节点
		
		queue.push(root);
		BinaryTreeNode<Integer> temp = null;
		for(int i=2;i<10;i=i+2){
			BinaryTreeNode<Integer> tmpNode1 = new BinaryTreeNode<>(i);
			BinaryTreeNode<Integer> tmpNode2 = new BinaryTreeNode<>(i+1);
			
			temp = queue.pop();
			
			temp.setLeft(tmpNode1);
			temp.setRight(tmpNode2);
			
			if(i != 4)
				queue.push(tmpNode1);
			queue.push(tmpNode2);
		}

		System.out.println("所有节点个数是：" + getNumbersByPreOrder(root));
		System.out.println("所有节点个数是：" + getNumbersByQueue(root));
		
	}

}
