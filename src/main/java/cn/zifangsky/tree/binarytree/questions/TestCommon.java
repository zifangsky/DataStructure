package cn.zifangsky.tree.binarytree.questions;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formattable;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TestCommon {
	
	/**
	 * 思路：依次比较即可
	 * @param a
	 * @param b
	 * @param c
	 */
	public void printMaxMin(int a,int b,int c){
		int max = a,min = a;
		
		if(b > max){
			max = b;
		}else{
			min = b;
		}
		
		if(c > max){
			max = c;
		}
		if(c < min){
			min = c;
		}
		
		System.out.println("最大值是： " + max + " 最小值是： " + min);
	}
	
	/**
	 * 检测一个时间是否在一个时间区间之内
	 * @param start 有效的开始时间
	 * @param end 有效的结束时间
	 * @param check 待检测时间
	 * @return
	 */
	public boolean checkValidDay(Date start,Date end,Date check){
		if(check.after(start) && check.before(end)){
			return true;
		}else{
			return false;
		}
	}
	
	@Test
	public void test() throws ParseException{
		String validStartDayStr = "20170501"; //有效的开始时间
		String validEndDayStr = "20171231"; //有效的结束时间
		
		String testDay1Str = "20160101"; //测试时间1
		String testDay2Str = "20170720"; //测试时间1
		String testDay3Str = "20181230"; //测试时间1
		
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		Date startDay = format.parse(validStartDayStr);
		Date endDay = format.parse(validEndDayStr);
		
		Date testDay1 = format.parse(testDay1Str);
		Date testDay2 = format.parse(testDay2Str);
		Date testDay3 = format.parse(testDay3Str);
		
		System.out.println(checkValidDay(startDay, endDay, testDay1));
		System.out.println(checkValidDay(startDay, endDay, testDay2));
		System.out.println(checkValidDay(startDay, endDay, testDay3));
	}
	
	@Test
	public void parseDate() throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = dateFormat.parse("2017-08-02");
		
		System.out.println(date);

	}
	
	@Test
	public void testMap(){
		DecimalFormat decimalFormat = new DecimalFormat("#0.00");
		
		String airPrice = "140.0";
		airPrice = decimalFormat.format(Double.valueOf(airPrice));
		System.out.println(airPrice);
		
		
	}
	
}
