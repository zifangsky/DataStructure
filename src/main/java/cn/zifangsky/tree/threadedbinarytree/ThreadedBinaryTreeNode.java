package cn.zifangsky.tree.threadedbinarytree;

/**
 * 中序线索二叉树的单个节点定义
 * @author zifangsky
 *
 * @param <K>
 */
public class ThreadedBinaryTreeNode<K extends Object> {
	private K data; // 数据
	private ThreadedBinaryTreeNode<K> left; // 左孩子节点
	private ThreadedBinaryTreeNode<K> right; // 右孩子节点

	/**
	 * leftTag如果为0，则left指向中序遍历的前驱节点
	 * leftTag如果为1，则left指向该节点的左孩子节点
	 */
	private int leftTag;
	/**
	 * rightTag如果为0，则left指向中序遍历的后继节点
	 * rightTag如果为1，则left指向中序遍历的右孩子节点
	 */
	private int rightTag;

	public ThreadedBinaryTreeNode(K data) {
		this.data = data;
	}

	public ThreadedBinaryTreeNode(K data, ThreadedBinaryTreeNode<K> left, ThreadedBinaryTreeNode<K> right) {
		this.data = data;
		this.left = left;
		this.right = right;
	}

	public ThreadedBinaryTreeNode(K data, ThreadedBinaryTreeNode<K> left, ThreadedBinaryTreeNode<K> right, int leftTag,
			int rightTag) {
		this.data = data;
		this.left = left;
		this.right = right;
		this.leftTag = leftTag;
		this.rightTag = rightTag;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	public ThreadedBinaryTreeNode<K> getLeft() {
		return left;
	}

	public void setLeft(ThreadedBinaryTreeNode<K> left) {
		this.left = left;
	}

	public ThreadedBinaryTreeNode<K> getRight() {
		return right;
	}

	public void setRight(ThreadedBinaryTreeNode<K> right) {
		this.right = right;
	}

	public int getLeftTag() {
		return leftTag;
	}

	public void setLeftTag(int leftTag) {
		this.leftTag = leftTag;
	}

	public int getRightTag() {
		return rightTag;
	}

	public void setRightTag(int rightTag) {
		this.rightTag = rightTag;
	}

}
