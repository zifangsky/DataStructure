package cn.zifangsky.tree.splaytree;

import java.util.Arrays;
import java.util.List;

/**
 * 伸展树基本用法的测试用例
 *
 * @author zifangsky
 * @date 2018/11/29
 * @since 1.0.0
 */
public class TestDemo {

    public static void main(String[] args) {
        //定义一个伸展树的实例
        SplayTree<Integer> splayTree = new SplayTree<>();
        //定义一些节点示例
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7);
        //将这些树插入到伸展树
        splayTree.insert(numbers);

        //遍历
        splayTree.forEach(element -> {
            System.out.println("Node:" + element);
        });

        System.out.println(splayTree.contains(1));

        //删除节点10
        splayTree.remove(7);

        System.out.println("此时最小元素：" + splayTree.findMin());
        System.out.println("此时最大元素：" + splayTree.findMax());
        System.out.println("此时元素 1 是否存在：" + splayTree.contains(1));
        System.out.println("此时元素 2 是否存在：" + splayTree.contains(2));
        System.out.println("此时的伸展树是否为空：" + splayTree.isEmpty());
        //清空伸展树
        splayTree.clear();

    }
}
