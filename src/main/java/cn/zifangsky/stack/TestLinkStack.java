package cn.zifangsky.stack;

import org.junit.Test;

public class TestLinkStack {

	@Test
	public void testLinkStack(){
		LinkStack<Integer> linkStack = new LinkStack<Integer>();
		linkStack.push(1);
		linkStack.push(2);
		linkStack.push(3);
		linkStack.push(4);
		linkStack.push(5);
		
		System.out.println(linkStack.pop());
		System.out.println(linkStack.top());
		System.out.println(linkStack.pop());
		System.out.println("剩余栈元素个数： " + linkStack.size());
	}
}
