package cn.zifangsky.stack;

/**
 * 栈的定义，以及栈的基本方法
 * @author zifangsky
 *
 * @param <K>
 */
public interface Stack<K> {
	/**
	 * 返回栈是否是空栈
	 */
	boolean isEmpty();

	/**
	 * 返回是否是满栈
	 */
	boolean isFull();

	default boolean contains(K k){return false;};
	
	/**
	 * 入栈操作
	 */
	void push(K data);
	
	/**
	 * 出栈操作
	 */
	K pop();
	
	/**
	 * 返回栈顶元素，但不删除
	 */
	K top();
	
	/**
	 * 返回存储在栈中的元素个数
	 */
	int size();
	
	/**
	 * 遍历操作
	 */
	void print();
	
	/**
	 * 删除整个栈
	 */
	void deleteStack();
}
