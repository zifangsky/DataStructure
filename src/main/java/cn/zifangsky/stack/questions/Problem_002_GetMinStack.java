package cn.zifangsky.stack.questions;

import org.junit.Test;

/**
 * 如何设计一个栈，使得getMinimum() 操作的时间复杂度为O(1)？
 * @author zifangsky
 *
 */
public class Problem_002_GetMinStack {

	@Test
	public void testMethod(){
		AdvancedStack stack = new AdvancedStack();
		stack.push(4);
		stack.push(3);
		stack.push(5);
		stack.push(2);
		stack.push(3);
		stack.push(1);
		
		stack.pop();
		System.out.println("当前栈的最小值： " + stack.getMinimum());
		
		stack.pop();
		stack.pop();
		System.out.println("当前栈的最小值： " + stack.getMinimum());
		
		stack.pop();
		stack.pop();
		System.out.println("当前栈的最小值： " + stack.getMinimum());
	}
}
