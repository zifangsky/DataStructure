package cn.zifangsky.graph;

/**
 * 图中的边
 *
 * @author zifangsky
 * @date 2019/1/22
 * @since 1.0.0
 */
public class Edge {
    /**
     * 起始顶点
     */
    private String startVertex;
    /**
     * 结束顶点
     */
    private String endVertex;
    /**
     * 边的权重（无向图的边的权重为1，两个顶点没有连接其权重为-1）
     */
    private Integer weight;

    /**
     * 默认权重为1的构造方法
     */
    public Edge(String startVertex, String endVertex) {
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.weight = 1;
    }

    public Edge(String startVertex, String endVertex, Integer weight) {
        this.startVertex = startVertex;
        this.endVertex = endVertex;
        this.weight = weight;
    }

    public String getStartVertex() {
        return startVertex;
    }

    public void setStartVertex(String startVertex) {
        this.startVertex = startVertex;
    }

    public String getEndVertex() {
        return endVertex;
    }

    public void setEndVertex(String endVertex) {
        this.endVertex = endVertex;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "startVertex='" + startVertex + '\'' +
                ", endVertex='" + endVertex + '\'' +
                ", weight=" + weight +
                '}';
    }
}
