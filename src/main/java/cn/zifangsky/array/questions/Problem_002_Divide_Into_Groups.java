package cn.zifangsky.array.questions;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 有6位同学前往书店购买一本书，他们六人分别带了37元、25元、21元、18元、17元和14元钱，
 * 到了书店以后，他们才知晓这本书的价格颇高，而他们自己带的钱不足以购买这本书。
 * 不过，他们6人中，其中有3人的钱凑在一起则正好可以买2本。此外，除去这3人，有2人的钱凑在一起刚好能买1本。
 * 请问，你能否根据以上信息推理出这本书的价格是多少呢?
 *
 * @author zifangsky
 * @date 2019/12/19
 * @since 1.0.0
 */
public class Problem_002_Divide_Into_Groups {

    @Test
    public void testMethods(){
        List<Integer> amountList = new ArrayList<>();
        amountList.add(37);
        amountList.add(25);
        amountList.add(21);
        amountList.add(18);
        amountList.add(17);
        amountList.add(14);


        Integer bookPrice = this.getUnitPrice(amountList);
        if(bookPrice != null){
            System.out.println("图书的单价是：" + bookPrice);
        }else{
            System.out.println("该题目无解！");
        }
    }

    /**
     * 计算图书单价
     * @param amountList 6位同学的金额集合
     */
    private Integer getUnitPrice(List<Integer> amountList){
        if(amountList == null || amountList.size() != 6){
            return null;
        }

        //图书单价
        Integer bookPrice;

        //排除2位同学后的其他4位同学的金额集合
        List<Integer> otherHeap;
        for(int i = 0; i < amountList.size(); i++){
            for(int j = (i + 1); j < amountList.size(); j++){
                otherHeap = new ArrayList<>(amountList);
                otherHeap.remove(amountList.get(i));
                otherHeap.remove(amountList.get(j));
                
                //1. 先通过2层循环找到两位同学，通过这2位同学的总金额假设一个图书单价
                bookPrice = amountList.get(i) + amountList.get(j);
                
                //2. 然后判断“其他4位同学中是否存在3位同学的钱凑在一起则正好可以买2本”
                if(this.checkAmountIsTheSame(otherHeap, bookPrice)){
                    return bookPrice;
                }
            }
        }
        
        return null;
    }

    /**
     * 判断“其他4位同学中是否存在3位同学的钱凑在一起则正好可以买2本”
     */
    private boolean checkAmountIsTheSame(List<Integer> list, Integer bookPrice){
        if(list == null || list.size() != 4){
            return false;
        }

        //1. 计算4位同学的总金额
        Integer allAmount = list.stream().reduce(0, (o1, o2) -> (o1 + o2));

        for(Integer temp : list){
            //2. 其中有3人的钱凑在一起则正好可以买2本
            if((allAmount - temp) == (bookPrice * 2)){
                return true;
            }
        }

        return false;
    }

}
