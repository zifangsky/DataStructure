package cn.zifangsky.array.questions;

import org.junit.Test;

import java.util.Arrays;

/**
 * 要求在不使用新数组和新集合的情况下（即只使用给出的数组，因数组数据比较大，且只能用一次循环）实现正数放到数组的前面，小于等于0的数放到数组的末尾
 *
 * @author zifangsky
 * @date 2019/8/7
 * @since 1.0.0
 */
public class Problem_001_MovePositiveIntegerToFront {

    public void sort(int[] array){
        int length = array.length;

        for(int i = 0, j = (length - 1); i < j;){
            //1. 将 i 向右移动，直到遇到小于等于0的数
            while (array[i] > 0){
                i++;
            }

            //2. 再将 j 向左移动，直到遇到大于0的数
            while (array[j] <= 0){
                j--;
            }

            //3. 将左边的小于等于0的数交换到右边
            if(i < j){
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }

    @Test
    public void testMethods() {
        int[] array = {-11, 3, -1, 0, 2, 1, -4, 2, 0, -2};

        //1. 排序
        sort(array);
        //2. 输出结果
        System.out.println(Arrays.toString(array));

    }

}
