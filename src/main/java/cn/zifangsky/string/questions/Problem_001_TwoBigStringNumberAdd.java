package cn.zifangsky.string.questions;

import org.junit.Test;

/**
 * 两个字符串格式的大数求和
 *
 * <ol>
 *     <li>输入：15 20；结果：35</li>
 *     <li>输入：150 20；结果：170</li>
 *     <li>输入：1515 250；结果：1765</li>
 * </ol>
 * @author zifangsky
 * @date 2020/8/20
 * @since 1.0.0
 */
public class Problem_001_TwoBigStringNumberAdd {

    /**
     * 测试代码
     */
    @Test
    public void testMethods(){
        System.out.println(this.twoBigStringNumberAdd("15", "20"));
        System.out.println(this.twoBigStringNumberAdd("150", "20"));
        System.out.println(this.twoBigStringNumberAdd("1515", "250"));
        System.out.println(this.twoBigStringNumberAdd("123456789", "9876543210"));
    }

    /**
     * 两个字符串格式的大数求和
     */
    private String twoBigStringNumberAdd(String num1, String num2){
        //1. 计算两个数字长度
        int len1 = num1.length();
        int len2 = num2.length();
        if(len1 <= 0 || len2 <= 0){
            throw new IllegalArgumentException("参数存在异常！");
        }

        //2. 定义结果收集器，以及进位
        StringBuilder sb = new StringBuilder();
        int carry = 0;

        //3. 模拟人工计算过程
        while (len1 > 0 || len2 > 0 || carry > 0){
            int x = (len1 > 0 ? (num1.charAt(len1 - 1) - '0') : 0);
            int y = (len2 > 0 ? (num2.charAt(len2 - 1) - '0') : 0);

            //3.1 当前位数之和 = 两个数相加 + 后一位的进位
            int sum = x + y + carry;
            //3.2 更新求和之后的当前位的数字
            sb.append(sum % 10);
            //3.3 更新进位
            carry = sum / 10;
            //3.4 更新下次计算的位数
            len1--;
            len2--;
        }

        return sb.reverse().toString();
    }

}
