package cn.zifangsky.sort;

import org.junit.Test;

import java.util.Arrays;

/**
 * 测试排序算法
 *
 * @author zifangsky
 * @date 2019/1/3
 * @since 1.0.0
 */
public class TestSort {

    /**
     * 测试冒泡法排序
     */
    @Test
    public void testBubbleSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = BubbleSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试选择法排序
     */
    @Test
    public void testSelectionSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = SelectionSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试插入法排序
     */
    @Test
    public void testInsertionSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = InsertionSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试希尔排序
     */
    @Test
    public void testShellSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = ShellSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试堆排序
     */
    @Test
    public void testHeapSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = HeapSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试归并排序
     */
    @Test
    public void testMergeSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

        int[] sortedArray = MergeSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }


    /**
     * 测试快速排序
     */
    @Test
    public void testQuickSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, -1, 6, 15};

//        int[] sortedArray = QuickSort.sort(array);
        int[] sortedArray = QuickSort.sort(array, 1);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试桶排序
     */
    @Test
    public void testBucketSort(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 15};

        int[] sortedArray = BucketSort.sort(array);
        System.out.println(Arrays.toString(sortedArray));
    }


    /**
     * 测试基数排序一：排序数字
     */
    @Test
    public void testRadixSort1(){
        int[] array = {5, 2, 6, 9, 7, 3, 8, 1, 4, 3, 0, 6, 15, 100, 1000};

        int[] sortedArray = RadixSort.sortNum1(array);
//        int[] sortedArray = RadixSort.sortNum2(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试基数排序二：排序定长的字符串
     */
    @Test
    public void testRadixSort2(){
        String[] array = {"abcd", "abdf", "bdac", "fdac", "dafc"};

//        String[] sortedArray = RadixSort.sortStr1(array, 4);
        String[] sortedArray = RadixSort.sortStr2(array, 4);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试基数排序三：排序不定长的字符串（以最高位为基准进行排序）
     */
    @Test
    public void testRadixSort3(){
        String[] array = {"bdac", "abc", "bad", "abf", "cadfc", "fdac", "cafc", "baddc", "acdfc"};

        String[] sortedArray = RadixSort.sortStr3(array);
        System.out.println(Arrays.toString(sortedArray));
    }

    /**
     * 测试基数排序四：排序不定长的字符串（以位数为基准进行排序）
     */
    @Test
    public void testRadixSort4(){
        String[] array = {"bdac", "abc", "bad", "abf", "cadfc", "fdac", "cafc", "cabcd", "baddc", "acdfb"};

        String[] sortedArray = RadixSort.sortStr4(array);
        System.out.println(Arrays.toString(sortedArray));
    }

}
