package cn.zifangsky.sort;

/**
 * 选择法排序
 *
 * @author zifangsky
 * @date 2019/1/3
 * @since 1.0.0
 */
public class SelectionSort {

    /**
     * 选择法排序
     * <p><b>时间复杂度：</b>O(n^2)</p>
     * @author zifangsky
     * @date 2019/1/3 11:03
     * @since 1.0.0
     * @param array 待排序数组
     */
    public static int[] sort(int[] array){
        if(array == null || array.length < 1){
            throw new IllegalArgumentException("Illegal initial array");
        }

        int length = array.length;
        for(int i = 0; i < length; i++){
            for(int j = (i + 1); j < length; j++) {
                //如果array[j] < array[i]，则交换位置
                if(array[j] < array[i]){
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }

}
