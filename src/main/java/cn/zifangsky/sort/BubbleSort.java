package cn.zifangsky.sort;

/**
 * 冒泡法排序
 *
 * @author zifangsky
 * @date 2018/12/26
 * @since 1.0.0
 */
public class BubbleSort {

    /**
     * 冒泡法排序
     * <p><b>时间复杂度：</b>O(n^2)</p>
     * @author zifangsky
     * @date 2019/1/3 10:58
     * @since 1.0.0
     * @param array 待排序数组
     */
    public static int[] sort(int[] array){
        if(array == null || array.length < 1){
            throw new IllegalArgumentException("Illegal initial array");
        }

        int length = array.length;
        for(int i = 0; i < length; i++){
            for(int j = (length - 1); j > i; j--) {
                //如果array[j] < array[j - 1]，则交换位置
                if(array[j] < array[j - 1]){
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }

        return array;
    }

}
