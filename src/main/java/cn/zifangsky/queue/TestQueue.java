package cn.zifangsky.queue;

import org.junit.Test;

public class TestQueue {

	@Test
	public void testArrayQueue(){
		ArrayQueue arrayQueue = new ArrayQueue(8);
		//入队
		arrayQueue.push(1);
		arrayQueue.push(2);
		arrayQueue.push(3);
		arrayQueue.push(4);
		
		System.out.println("队列大小： " + arrayQueue.size());
		
		//出队
		System.out.println("出队：" + arrayQueue.pop());
		System.out.println("出队：" + arrayQueue.pop());
		System.out.println("出队：" + arrayQueue.pop());
		
		System.out.println("队列大小： " + arrayQueue.size());
		
		System.out.println("出队：" + arrayQueue.pop());
		System.out.println("出队：" + arrayQueue.pop()); //抛出异常
	}
	
	@Test
	public void testDynArrayQueue(){
		DynArrayQueue dynArrayQueue = new DynArrayQueue();
		//入队
		dynArrayQueue.push(1);
		dynArrayQueue.push(2);
		dynArrayQueue.push(3);
		dynArrayQueue.push(4);
		
		System.out.println("队列大小： " + dynArrayQueue.size());
		
		//出队
		System.out.println("出队：" + dynArrayQueue.pop());
		System.out.println("出队：" + dynArrayQueue.pop());
		System.out.println("出队：" + dynArrayQueue.pop());
		
		System.out.println("队列大小： " + dynArrayQueue.size());
		
		dynArrayQueue.push(5);
		System.out.println("出队：" + dynArrayQueue.pop());
		System.out.println("出队：" + dynArrayQueue.pop());
	}
	
	@Test
	public void testLinkQueue(){
		LinkQueue<Integer> linkQueue = new LinkQueue<>();
		//入队
		linkQueue.push(1);
		linkQueue.push(2);
		linkQueue.push(3);
		linkQueue.push(4);
		
		System.out.println("队列大小： " + linkQueue.size());
		
		//出队
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());
		
		System.out.println("队列大小： " + linkQueue.size());
		
		linkQueue.push(5);
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());
	}

	/**
	 * 测试优先队列
	 */
	@Test
	public void testPriorityQueue(){
		Queue<Integer> linkQueue = new PriorityQueue<>(PriorityQueue.Mode.MIN);
		//入队
		linkQueue.push(3);
		linkQueue.push(1);
		linkQueue.push(2);
		linkQueue.push(4);

		System.out.println("队列大小： " + linkQueue.size());

		//出队
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());

		System.out.println("队列大小： " + linkQueue.size());

		linkQueue.push(5);
		System.out.println("出队：" + linkQueue.pop());
		System.out.println("出队：" + linkQueue.pop());
	}


}
