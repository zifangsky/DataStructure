package cn.zifangsky.queue.questions;

import org.junit.Test;

import cn.zifangsky.queue.LinkQueue;
import cn.zifangsky.queue.Queue;
import cn.zifangsky.stack.LinkStack;
import cn.zifangsky.stack.Stack;

/**
 * 逆置队列元素
 * @author zifangsky
 *
 */
public class Problem_001_ReverseQueue {
	
	/**
	 * 思路：使用栈辅助操作即可
	 * @时间复杂度 O(n)
	 * @param queue
	 * @return
	 */
	public Queue<Integer> reverseQueue(Queue<Integer> queue){
		Stack<Integer> stack = new LinkStack<>();

		while (!queue.isEmpty()) {
			stack.push(queue.pop());
		}
		
		while (!stack.isEmpty()) {
			queue.push(stack.pop());
		}
		
		return queue;
	}

	@Test
	public void testMethods(){
		Queue<Integer> queue = new LinkQueue<>();
		queue.push(1);
		queue.push(2);
		queue.push(3);
		queue.push(4);
		queue.push(5);
		queue.push(6);
		
		queue.print(data -> System.out.print(data + " "));
		System.out.println();
		
		queue = reverseQueue(queue);
		queue.print(data -> System.out.print(data + " "));
	}
}
