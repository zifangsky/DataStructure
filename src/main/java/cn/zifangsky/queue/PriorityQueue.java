package cn.zifangsky.queue;

import cn.zifangsky.heap.BinaryHeap;
import cn.zifangsky.heap.BinaryMaxHeap;
import cn.zifangsky.heap.BinaryMinHeap;

import java.util.function.Consumer;

/**
 * 优先队列
 *
 * @author zifangsky
 * @date 2019/1/25
 * @since 1.0.0
 */
public class PriorityQueue<T extends Comparable<? super T>> implements Queue<T>{
    /**
     * 优先队列的模式
     */
    public enum Mode{
        /**
         * 最大优先队列
         */
        MAX,
        /**
         * 最小优先队列
         */
        MIN;
    }

    /**
     * 二叉堆
     */
    private BinaryHeap<T> heap;

    /**
     * 优先队列的模式
     */
    private Mode mode;

    public PriorityQueue(Mode mode) {
        if(mode == Mode.MIN){
            this.heap = new BinaryMinHeap<>();
        }else{
            this.heap = new BinaryMaxHeap<>();
        }
        this.mode = mode;
    }

    public PriorityQueue(Mode mode, int initialCapacity) {
        if(mode == Mode.MIN){
            this.heap = new BinaryMinHeap<>(initialCapacity);
        }else{
            this.heap = new BinaryMaxHeap<>(initialCapacity);
        }
        this.mode = mode;
    }

    @Override
    public boolean isEmpty() {
        return heap.isEmpty();
    }

    @Override
    public int size() {
        return heap.size();
    }

    @Override
    public void push(T data) {
        heap.insert(data);
    }

    @Override
    public void pushIfNotExist(T data) {
        if(!heap.contains(data)){
            heap.insert(data);
        }
    }

    @Override
    public T pop() {
        return heap.deleteFirst();
    }

    @Override
    public T top() {
        return heap.getFirst();
    }

    @Override
    public void clear() {
        heap.clear();
    }

    @Override
    public void print(Consumer<T> consumer) {

    }

    public T[] toArray(T[] a){
        return heap.toArray(a);
    }

    @Override
    public void deleteQueue() {
        heap.clear();
    }

    public Mode getMode() {
        return mode;
    }
}
