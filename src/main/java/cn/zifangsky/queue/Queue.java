package cn.zifangsky.queue;

import java.util.function.Consumer;

/**
 * 队列的定义，以及队列的基本方法
 * @author zifangsky
 *
 */
public interface Queue<K> {
	/**
	 * 返回队列是否为空
	 */
	boolean isEmpty();

	/**
	 * 返回存储在队列的元素个数
	 */
	int size();

	/**
	 * 入队操作
	 */
	void push(K data);

	default void pushIfNotExist(K data){};

	/**
	 * 出队操作
	 */
	K pop();

	/**
	 * 返回队首的元素，但不删除
	 */
	K top();

    /**
     * 清空队列
     */
	void clear();

	/**
	 * 遍历操作
	 */
	void print(Consumer<K> consumer);

	/**
	 * 删除整个队列
	 */
	void deleteQueue();
}
