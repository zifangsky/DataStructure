package cn.zifangsky.disjoint;

import org.junit.Test;

/**
 * 测试用例
 *
 * @author zifangsky
 * @date 2019/1/16
 * @since 1.0.0
 */
public class TestDisJoint {

    /**
     * 测试基本的不相交集
     */
    @Test
    public void testDisjSets(){
        DisjSets disjSets = new DisjSets(15);

        disjSets.union(4, 5);
        disjSets.union(6, 7);
        disjSets.union(6, 8);
        disjSets.union(2, 4);
        disjSets.union(4, 6);
        disjSets.union(3, 8);

        System.out.println("1的根节点：" + disjSets.find(1));
        System.out.println("4的根节点：" + disjSets.find(4));
        System.out.println("3和4是否相连：" + disjSets.isConnected(3, 4));
    }

}
