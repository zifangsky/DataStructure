package cn.zifangsky.disjoint.questions;

import cn.zifangsky.disjoint.DisjSets;

/**
 * 朋友
 *
 * Today is Ignatius' birthday. He invites a lot of friends.
 * Now it's dinner time. Ignatius wants to know how many tables he needs at least.
 * You have to notice that not all the friends know each other, and all the friends do not want to stay with strangers.
 *
 * One important rule for this problem is that if I tell you A knows B,
 * and B knows C, that means A, B, C know each other, so they can stay in one table.
 *
 * For example: If I tell you A knows B, B knows C, and D knows E, so A, B, C can stay in one table,
 * and D, E have to stay in the other one. So Ignatius needs 2 tables at least.
 *
 * @author zifangsky
 * @date 2019/1/17
 * @since 1.0.0
 */
public class Problem_002_Friends {

    public static void main(String[] args) {
        DisjSets disjSets = new DisjSets(5);

        disjSets.union(0, 1);
        disjSets.union(1, 2);
        disjSets.union(3, 4);

        System.out.println("需要安排 " + disjSets.getGroupNum() + " 个桌子");
    }

}
