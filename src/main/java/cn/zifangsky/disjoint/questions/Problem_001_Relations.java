package cn.zifangsky.disjoint.questions;

import cn.zifangsky.disjoint.DisjSets;

/**
 * 亲戚（Relations）
 *
 * 或许你并不知道，你的某个朋友是你的亲戚。他可能是你的曾祖父的外公的女婿的外甥的表姐的孙子。
 * 如果能得到完整的家谱，判断两个人是否亲戚应该是可行的，但如果两个人的最近公共祖先与他们相隔好几代，
 * 使得家谱十分庞大，那么检验亲戚关系实非人力所能及.在这种情况下，最好的帮手就是计算机。
 *
 * 为了将问题简化，你将得到一些亲戚关系的信息，如同Marry和Tom是亲戚，Tom和B en是亲戚，等等。
 * 从这些信息中，你可以推出Marry和Ben是亲戚。请写一个程序，对于我们的关心的亲戚关系的提问，以最快的速度给出答案。
 *
 * 参考输入输出格式 输入由两部分组成。
 * 第一部分以N，M开始。N为问题涉及的人的个数(1 ≤ N ≤ 20000)。这些人的编号为1,2,3,…,N。下面有M行(1 ≤ M ≤ 1000000)，每行有两个数ai, bi，表示已知ai和bi是亲戚.
 * 第二部分以Q开始。以下Q行有Q个询问(1 ≤ Q ≤ 1 000 000)，每行为ci, di，表示询问ci和di是否为亲戚。
 * 对于每个询问ci, di，若ci和di为亲戚，则输出Yes，否则输出No。
 *
 * @author zifangsky
 * @date 2019/1/17
 * @since 1.0.0
 */
public class Problem_001_Relations {

    public static void main(String[] args) {
        DisjSets disjSets = new DisjSets(100);

        disjSets.union(2, 4);
        disjSets.union(5, 7);
        disjSets.union(1, 3);
        disjSets.union(8, 9);
        disjSets.union(1, 2);
        disjSets.union(5, 6);
        disjSets.union(2, 3);

        System.out.println("3和4是否是亲戚：" + disjSets.isConnected(3, 4));
        System.out.println("7和10是否是亲戚：" + disjSets.isConnected(7, 10));
        System.out.println("8和9是否是亲戚：" + disjSets.isConnected(8, 9));
    }

}
