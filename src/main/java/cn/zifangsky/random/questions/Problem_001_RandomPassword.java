package cn.zifangsky.random.questions;

import org.junit.Test;

import java.util.Random;

/**
 * 生成指定位数的随机复杂密码（至少包含一个大写字母、小写字母、特殊字符、数字）
 *
 * @author zifangsky
 * @date 2020/5/11
 * @since 1.0.0
 */
public class Problem_001_RandomPassword {
    private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

    /**
     * 查找一个char数组中还没有填充字符的位置
     */
    private int nextIndex(char[] chars, Random rnd) {
        int index = rnd.nextInt(chars.length);
        while (chars[index] != 0) {
            index = rnd.nextInt(chars.length);
        }
        return index;
    }

    /**
     * 返回一个随机的特殊字符
     */
    private char nextSpecialChar(Random rnd) {
        return SPECIAL_CHARS.charAt(rnd.nextInt(SPECIAL_CHARS.length()));
    }

    /**
     * 返回一个随机的大写字母
     */
    private char nextUpperLetter(Random rnd) {
        return (char) ('A' + rnd.nextInt(26));
    }

    /**
     * 返回一个随机的小写字母
     */
    private char nextLowerLetter(Random rnd) {
        return (char) ('a' + rnd.nextInt(26));
    }

    /**
     * 返回一个随机的数字
     */
    private char nextNumLetter(Random rnd) {
        return (char) ('0' + rnd.nextInt(10));
    }

    /**
     * 返回一个随机的字符
     */
    private char nextChar(Random rnd) {
        switch (rnd.nextInt(4)) {
            case 0:
                return (char) ('a' + rnd.nextInt(26));
            case 1:
                return (char) ('A' + rnd.nextInt(26));
            case 2:
                return (char) ('0' + rnd.nextInt(10));
            default:
                return SPECIAL_CHARS.charAt(rnd.nextInt(SPECIAL_CHARS.length()));
        }
    }

    /**
     * 生成指定位数的随机数
     */
    private String randomPassword(int length) {
        if(length < 4){
            return "";
        }
        char[] chars = new char[length];
        Random rnd = new Random();

        //1. 至少生成一个大写字母、小写字母、特殊字符、数字
        chars[nextIndex(chars, rnd)] = nextSpecialChar(rnd);
        chars[nextIndex(chars, rnd)] = nextUpperLetter(rnd);
        chars[nextIndex(chars, rnd)] = nextLowerLetter(rnd);
        chars[nextIndex(chars, rnd)] = nextNumLetter(rnd);

        //2. 填补其他位置的字符
        for (int i = 0; i < length; i++) {
            if (chars[i] == 0) {
                chars[i] = nextChar(rnd);
            }
        }

        //3. 返回结果
        return new String(chars);
    }
    

    /**
     * 测试代码
     */
    @Test
    public void testMethods(){
        for (int i = 4; i <= 15; i++){
            System.out.println(randomPassword(i));
        }
    }
}
