package cn.zifangsky.hashtable.fifo;

import org.junit.Test;

/**
 * 测试用例
 *
 * @author zifangsky
 * @date 2020/6/24
 * @since 1.0.0
 */
public class TestDemo {

    @Test
    public void testMethod1() {
        FIFOCache<String, Integer> fifoCache = new FIFOCache<>(3, false);

        fifoCache.put("A", 1);
        fifoCache.put("B", 2);
        fifoCache.put("C", 3);
        System.out.println("B的值是：" + fifoCache.get("B"));
        System.out.println("A的值是：" + fifoCache.get("A"));

        //此时因为缓存容量不够A会被清除
        fifoCache.put("D", 4);
        System.out.println("D的值是：" + fifoCache.get("D"));
        System.out.println("A的值是：" + fifoCache.get("A"));

        fifoCache.remove("C");
        fifoCache.remove("B");
        System.out.println("当前CACHE中的键值对数量：" + fifoCache.size());
    }

    @Test
    public void testMethod2() throws Exception {
        FIFOCache<String, String> fifoCache = new FIFOCache<>(3);

        fifoCache.put("A", "10", -1);
        fifoCache.put("B", "20", 5);
        fifoCache.put("C", "30", 10);
        System.out.println("B的值是：" + fifoCache.get("B"));
        System.out.println("A的值是：" + fifoCache.get("A"));

        //暂停6秒钟，让B过期被清除
        Thread.sleep(6000);
        fifoCache.put("D", "40", 30);
        System.out.println("********** 第一次暂停6秒钟后 **********");
        fifoCache.forEach((key, value) -> System.out.println(key + "-->" + value));

        //再暂停6秒钟，让C过期被清除
        Thread.sleep(6000);
        System.out.println("********** 第二次暂停6秒钟后 **********");
        fifoCache.forEach((key, value) -> System.out.println(key + "-->" + value));

        //再PUT两个键值对，此时因为缓存容量不够A会被清除
        fifoCache.put("E", "50", 30);
        fifoCache.put("F", "60", 30);
        System.out.println("********** 第三次结果 **********");
        fifoCache.forEach((key, value) -> System.out.println(key + "-->" + value));
    }

}
