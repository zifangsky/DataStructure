package cn.zifangsky.hashtable.slidingtime;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 测试用例
 *
 * @author zifangsky
 * @date 2020/7/1
 * @since 1.0.0
 */
public class TestDemo {

    @Test
    public void testMethod1() throws InterruptedException {
        SlidingTimeCache<String> cache = new SlidingTimeCache<>();
        int time = 10;
        TimeUnit unit= TimeUnit.SECONDS;

        List<Pair<String, String>> pairs = Arrays.asList(Pair.of("a", "down"), Pair.of("b", "down"),
                Pair.of("c", "down"), Pair.of("b", "up"));
        cache.addBatch(pairs, time, unit);

        unit.sleep(8);
        //get  null
        Map<String, List<String>> expiredData = cache.getExpiredData();

        //set
        pairs = Arrays.asList(Pair.of("c", "up"), Pair.of("d", "down"), Pair.of("e", "down"));
        cache.addBatch(pairs, time, unit);

        unit.sleep(4);
        //get  a,b,c
        expiredData = cache.getExpiredData();

        //set
        pairs = Arrays.asList(Pair.of("b", "down"), Pair.of("e", "up"));
        cache.addBatch(pairs, time, unit);

        unit.sleep(1);
        pairs = Arrays.asList(Pair.of("b", "up"));
        cache.addBatch(pairs, time, unit);

        unit.sleep(10);
        //get  b,d,e
        expiredData = cache.getExpiredData();

        unit.sleep(2);
        expiredData = cache.getExpiredData();
        System.out.println(expiredData.keySet());
    }
}
