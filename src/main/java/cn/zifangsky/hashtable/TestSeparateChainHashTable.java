package cn.zifangsky.hashtable;

import org.junit.Test;

/**
 * 基于“分离链接法”实现的 Hash Table的测试用例
 *
 * @author zifangsky
 * @date 2018/12/12
 * @since 1.0.0
 */
public class TestSeparateChainHashTable {

    public static void main(String[] args) {
        SeparateChainHashTable<Integer,String> table = new SeparateChainHashTable<>(4);
        table.put(1, "a");
        table.put(5, "b");
        table.put(6, "c");
        table.put(17, "d");
        table.put(21, "e");
        table.put(21, "f");
        table.put(33, "g");
        table.remove(17);

//        System.out.println(table.get(6));
        System.out.println(table.containsKey(33));
        table.forEach((key, value) -> {
            System.out.println(key + "=" + value);
        });
    }

    @Test
    public void test(){
        int a = 21;
        int b = 16;
        int result = a & b;
        System.out.println(Integer.toBinaryString(result));
    }
}
