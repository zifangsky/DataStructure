package cn.zifangsky.hashtable.lru;

import org.junit.Test;

/**
 * 测试用例
 *
 * @author zifangsky
 * @date 2020/6/25
 * @since 1.0.0
 */
public class TestDemo {

    @Test
    public void testMethod1() {
        LRUCache<String, Integer> lruCache = new LRUCache<>(3, false);

        lruCache.put("A", 1);
        lruCache.put("B", 2);
        lruCache.put("C", 3);
        System.out.println("B的值是：" + lruCache.get("B"));
        System.out.println("A的值是：" + lruCache.get("A"));

        //此时因为缓存容量不够C会被清除
        lruCache.put("D", 4);
        lruCache.forEach((key, value) -> System.out.println(key + "-->" + value));

        lruCache.remove("B");
        System.out.println("当前CACHE中的键值对数量：" + lruCache.size());
    }

    @Test
    public void testMethod2() throws Exception {
        LRUCache<String, String> lruCache = new LRUCache<>(3);

        lruCache.put("A", "10", -1);
        lruCache.put("B", "20", 5);
        lruCache.put("C", "30", 30);
        System.out.println("B的值是：" + lruCache.get("B"));
        System.out.println("A的值是：" + lruCache.get("A"));

        //暂停6秒钟，让B过期被清除
        Thread.sleep(6000);
        lruCache.put("D", "40", 30);
        System.out.println("********** 暂停6秒钟后 **********");
        lruCache.forEach((key, value) -> System.out.println(key + "-->" + value));

        //此时因为缓存容量不够C会被清除
        lruCache.put("E", "50", 30);
        System.out.println("********** 第二次结果 **********");
        lruCache.forEach((key, value) -> System.out.println(key + "-->" + value));
    }

}
