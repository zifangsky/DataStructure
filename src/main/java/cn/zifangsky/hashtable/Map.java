package cn.zifangsky.hashtable;

import java.util.function.BiConsumer;

/**
 * 哈希表的抽象接口
 *
 * @author zifangsky
 * @date 2019/1/28
 * @since 1.0.0
 */
public interface Map<K, V> {
    /**
     * 返回当前 Hash Table 存储的键值对的数量
     */
    int size();

    /**
     * 返回当前 Hash Table 是否为空
     */
    boolean isEmpty();

    /**
     * 返回当前 Hash Table 是否包含某个KEY
     * @param key KEY
     * @return V
     */
    boolean containsKey(K key);

    /**
     * 根据KEY查找VALUE
     * @param key KEY
     * @return V
     */
    V get(K key);

    /**
     * 存储某个键值对
     * @param key KEY
     * @param value VALUE
     */
    void put(K key, V value);

    /**
     * 移除某个键值对
     * @param key KEY
     */
    void remove(K key);

    /**
     * 清空Hash Table
     */
    void clear();

    /**
     * 遍历Hash Table
     * @param action action
     */
    void forEach(BiConsumer<? super K, ? super V> action);
}
