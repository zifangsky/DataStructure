package cn.zifangsky.heap;

import org.junit.Test;

/**
 * 堆相关测试代码
 *
 * @author zifangsky
 * @date 2018/12/26
 * @since 1.0.0
 */
public class TestBinaryHeap {

    /**
     * 测试最小堆
     */
    @Test
    public void testBinaryMinHeap(){
        Integer[] initialArray = {5, 2, 6, 9, 7, 3, 8, 1, 4};

        BinaryMinHeap<Integer> heap = new BinaryMinHeap<>(initialArray);

        System.out.println(heap.getFirst());
        System.out.println(heap.deleteFirst());

        heap.insert(1);
        System.out.println(heap.isEmpty());
    }

    /**
     * 测试最大堆
     */
    @Test
    public void testBinaryMaxHeap(){
        Integer[] initialArray = {5, 2, 6, 9, 7, 3, 8, 1, 4};

        BinaryMaxHeap<Integer> heap = new BinaryMaxHeap<>(initialArray);

        System.out.println(heap.getFirst());
        System.out.println(heap.deleteFirst());

        heap.insert(9);
        System.out.println(heap.isEmpty());
    }

}
