package cn.zifangsky.heap;

/**
 * 二叉堆
 *
 * @author zifangsky
 * @date 2018/12/25
 * @since 1.0.0
 */
public interface BinaryHeap<T extends Comparable<? super T>> {

    /**
     * 返回当前二叉堆的数量
     */
    int size();

    /**
     * 判断当前二叉堆是否为空
     * @author zifangsky
     * @date 2018/12/25 20:10
     * @since 1.0.0
     * @return boolean
     */
    boolean isEmpty();

    /**
     * 判断当前二叉堆是否存在某个值
     * @author zifangsky
     * @date 2018/12/25 20:10
     * @since 1.0.0
     * @return boolean
     */
    default boolean contains(T data){return false;};

    /**
     * 清空当前二叉堆
     * @author zifangsky
     * @date 2018/12/26 14:49
     * @since 1.0.0
     */
    void clear();

    /**
     * 插入
     * @author zifangsky
     * @date 2018/12/25 20:07
     * @since 1.0.0
     * @param data 待插入数据
     */
    void insert(Object data);

    /**
     * 查找最小（最大）元素
     * @author zifangsky
     * @date 2018/12/26 14:49
     * @since 1.0.0
     * @return T
     */
    T getFirst();

    /**
     * 删除最小（最大）元素
     * @author zifangsky
     * @date 2018/12/25 20:08
     * @since 1.0.0
     * @return T
     */
    T deleteFirst();

    /**
     * 返回整个堆代表的数组
     * @author zifangsky
     * @date 2020/5/14 11:57
     * @since 1.0.0
     * @return T[]
     */
    Object[] toArray();

    /**
     * 返回整个堆代表的数组
     * @author zifangsky
     * @date 2020/5/14 11:57
     * @since 1.0.0
     * @param a 数组
     * @return T[]
     */
    T[] toArray(T[] a);
}
