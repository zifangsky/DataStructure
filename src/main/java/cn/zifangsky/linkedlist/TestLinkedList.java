package cn.zifangsky.linkedlist;

import org.junit.Test;

/**
 * 测试{@link LinkedList}
 *
 * @author zifangsky
 * @date 2019/9/5
 * @since 1.0.0
 */
public class TestLinkedList {

    @Test
    public void test(){
        LinkedList<String> list = new LinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("e");
        list.add("f");

        list.add(3, "d");
        list.offerFirst("A");
        list.offer("g");
        list.offer("h");
        list.poll();
        list.pollLast();

        list.forEach(System.out::println);
    }

}
