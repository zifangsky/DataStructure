package cn.zifangsky.linkedlist;

/**
 * 单链表的定义
 * 
 * @author zifangsky
 * @param <K>
 */
public class SinglyNode<K extends Object> {
	private K data; // 数据
	private SinglyNode<K> next; // 该节点的下个节点

	public SinglyNode(K data) {
		this.data = data;
	}

	public SinglyNode(K data, SinglyNode<K> next) {
		this.data = data;
		this.next = next;
	}

	public K getData() {
		return data;
	}

	public void setData(K data) {
		this.data = data;
	}

	public SinglyNode<K> getNext() {
		return next;
	}

	public void setNext(SinglyNode<K> next) {
		this.next = next;
	}

	@Override
	public String toString() {
		return "SinglyNode [data=" + data + "]";
	}

}
