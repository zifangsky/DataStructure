package cn.zifangsky.linkedlist.questions;

import org.junit.Test;

import cn.zifangsky.linkedlist.CircularNode;

/**
 * 约瑟夫环问题
 * @author zifangsky
 *
 */
public class Problem_014_JosephRing {
	
	/**
	 * 
	 * @param N 人数
	 * @param M 每次数数个数
	 */
	public void getLastPerson(int N,int M){
		//构建一个环
		CircularNode headNode = new CircularNode(1);
		
		CircularNode currentNode = headNode;
		for(int i=2;i<=N;i++){
			CircularNode tmpNode = new CircularNode(i);
			currentNode.setNext(tmpNode);
			currentNode = tmpNode;
			
			if(i == N){
				currentNode.setNext(headNode);
			}
		}

		//当链表大于一个节点时一直循环排除下去
		while(headNode.getNext() != headNode){
			for(int i=1;i<M;i++){
				headNode = headNode.getNext();
			}
			headNode.setNext(headNode.getNext().getNext()); //排除headNode.getNext()这个人
		}
		System.out.println("剩下的人是： " + headNode.getData());
	}

	@Test
	public void testMethods(){
		getLastPerson(5,3);
	}
	
}
